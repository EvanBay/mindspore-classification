from typing import Union, Optional

from mindspore import nn
from mindspore import Tensor, dtype
from mindspore import ops
import mindspore.nn.probability.distribution as msd


class SqueezeExcite(nn.Cell):

    def __init__(self,
                 in_chs: int,
                 reduce_chs: int,
                 act_fn: Union[str, nn.Cell] = nn.ReLU,
                 gate_fn: Union[str, nn.Cell] = nn.Sigmoid
                 ) -> None:
        super(SqueezeExcite, self).__init__()
        self.act_fn = nn.get_activation(act_fn) if isinstance(act_fn, str) else act_fn()
        self.gate_fn = nn.get_activation(gate_fn) if isinstance(gate_fn, str) else gate_fn()
        reduce_chs = reduce_chs or in_chs
        self.conv_reduce = nn.Conv2d(in_channels=in_chs,
                                     out_channels=reduce_chs,
                                     kernel_size=1,
                                     has_bias=True,
                                     pad_mode='pad'
                                     )
        self.conv_expand = nn.Conv2d(in_channels=reduce_chs,
                                     out_channels=in_chs,
                                     kernel_size=1,
                                     has_bias=True,
                                     pad_mode='pad'
                                     )
        self.avg_global_pool = ops.ReduceMean(keep_dims=True)

    def construct(self, x) -> Tensor:
        """Squeeze-excite construct."""
        x_se = self.avg_global_pool(x, (2, 3))
        x_se = self.conv_reduce(x_se)
        x_se = self.act_fn(x_se)
        x_se = self.conv_expand(x_se)
        x_se = self.gate_fn(x_se)
        x = x * x_se
        return x


class DropPath(nn.Cell):
    """
    Drop paths (Stochastic Depth) per sample  (when applied in main path of residual blocks).
    """

    def __init__(self, drop_rate=0., seed=0):
        super(DropPath, self).__init__()
        self.keep_prob = 1 - drop_rate
        self.rand = ops.UniformReal(seed=seed)
        self.shape = ops.Shape()
        self.floor = ops.Floor()
        self.div = ops.Div()

    def construct(self, x):
        if self.keep_prob == 1.0 or not self.training:
            return x
        x_shape = self.shape(x)
        random_tensor = self.rand((x_shape[0], 1, 1))
        random_tensor = random_tensor + self.keep_prob
        random_tensor = self.floor(random_tensor)
        x = self.div(x, self.keep_prob)
        x = x * random_tensor

        return x


class StochasticDepthDrop(nn.Cell):
    """
    DropConnect function.

    Args:
        keep_prob (int): Drop rate of the MBConv Block. Default: 0.

    Return:
        Tensor

    Example:
        >>> import mindspore
        >>> from mindspore import Tensor
        >>> x = Tensor(((20, 16), (50, 50)), mindspore.float32)
        >>> StochasticDepthDrop(0.8)(x)
    """
    def __init__(self,
                 keep_prob: Optional[float] = None
                 ):
        super(StochasticDepthDrop, self).__init__()
        self.drop_rate = keep_prob
        self.bernoulli = msd.Bernoulli(probs=self.drop_rate, dtype=dtype.int32)

    def construct(self, x: Tensor):
        if not self.training or self.drop_rate == 0.:
            return x
        return x * self.bernoulli.sample((x.shape[0],) + (1,) * (x.ndim-1))
