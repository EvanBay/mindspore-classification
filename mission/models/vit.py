from typing import Optional
import numpy as np

import mindspore as ms
import mindspore.nn as nn
import mindspore.ops as ops
from mindspore import Tensor
from mindspore.common.initializer import initializer, Normal
from mindspore.common.parameter import Parameter

from .layers.blocks import DropPath
from .utils import load_pretrained
from .registry import register_model
from mission.data.constants import IMAGENET_DEFAULT_MEAN, IMAGENET_DEFAULT_STD, DEFAULT_CROP_PCT

'''
model zoo的vit写的是配置文件的写法，可读性很差，精度也差8个点多，没有参考
下面写的vit是参考torchvision以及rwrightman来写的，rwrightman有整套的预训练权重，后面的预训练权重也方便转成mindspore的
之前都验证过
'''

__all__ = ['PatchEmbedding', 'Attention', 'FeedForward', 'ResidualCell', 'TransformerEncoder', 'ViT']


def _cfg(url='', **kwargs):
    return {
        'url': url,
        'num_classes': 1000,
        'dataset_transform': {
            'transforms_imagenet_train': {
                'image_resize': 224,
                'scale': (0.08, 1.0),
                'ratio': (0.75, 1.333),
                'hflip': 0.5,
                'interpolation': 'bilinear',
                'mean': IMAGENET_DEFAULT_MEAN,
                'std': IMAGENET_DEFAULT_STD,
            },
            'transforms_imagenet_eval': {
                'image_resize': 224,
                'crop_pct': DEFAULT_CROP_PCT,
                'interpolation': 'bilinear',
                'mean': IMAGENET_DEFAULT_MEAN,
                'std': IMAGENET_DEFAULT_STD,
            },
        },
        'first_conv': 'conv1', 'classifier': 'fc',
        **kwargs
    }


default_cfgs = {
    'vit_b_16_224': _cfg(url=''),
    'vit_b_32_224': _cfg(url=''),
    'vit_l_16_224': _cfg(url=''),
    'vit_l_32_224': _cfg(url='')
}


class PatchEmbedding(nn.Cell):

    def __init__(self,
                 image_size: int = 224,
                 patch_size: int = 16,
                 embed_dim: int = 768,
                 input_channels: int = 3):
        super(PatchEmbedding, self).__init__()
        self.image_size = image_size
        self.patch_size = patch_size
        self.num_patches = (image_size // patch_size) ** 2

        self.conv = nn.Conv2d(input_channels, embed_dim, kernel_size=patch_size, stride=patch_size, has_bias=True)
        self.reshape = ops.Reshape()
        self.transpose = ops.Transpose()

    def construct(self, x):
        """Path Embedding construct."""
        x = self.conv(x)
        b, c, h, w = x.shape
        x = self.reshape(x, (b, c, h * w))
        x = self.transpose(x, (0, 2, 1))

        return x


class Attention(nn.Cell):

    def __init__(self,
                 dim: int,
                 num_heads: int = 8,
                 drop_rate: float = 0.,
                 attention_drop_rate: float = 0.):
        super(Attention, self).__init__()
        assert dim % num_heads == 0, 'dim should be divisible by num_heads.'
        self.num_heads = num_heads
        head_dim = dim // num_heads
        self.scale = Tensor(head_dim ** -0.5)

        self.qkv = nn.Dense(dim, dim * 3)
        self.attn_drop = nn.Dropout(1 - attention_drop_rate)
        self.out = nn.Dense(dim, dim)
        self.out_drop = nn.Dropout(1 - drop_rate)

        self.mul = ops.Mul()
        self.reshape = ops.Reshape()
        self.transpose = ops.Transpose()
        self.unstack = ops.Unstack(axis=0)
        self.attn_matmul_v = ops.BatchMatMul()
        self.q_matmul_k = ops.BatchMatMul(transpose_b=True)
        self.softmax = nn.Softmax(axis=-1)

    def construct(self, x):
        """Attention construct."""
        b, n, c = x.shape
        qkv = self.qkv(x)
        qkv = self.reshape(qkv, (b, n, 3, self.num_heads, c // self.num_heads))
        qkv = self.transpose(qkv, (2, 0, 3, 1, 4))
        q, k, v = self.unstack(qkv)

        attn = self.q_matmul_k(q, k)
        attn = self.mul(attn, self.scale)
        attn = self.softmax(attn)
        attn = self.attn_drop(attn)

        out = self.attn_matmul_v(attn, v)
        out = self.transpose(out, (0, 2, 1, 3))
        out = self.reshape(out, (b, n, c))
        out = self.out(out)
        out = self.out_drop(out)

        return out


class FeedForward(nn.Cell):

    def __init__(self,
                 in_features: int,
                 hidden_features: Optional[int] = None,
                 out_features: Optional[int] = None,
                 activation: nn.Cell = nn.GELU,
                 drop_rate: float = 0.):
        super(FeedForward, self).__init__()
        out_features = out_features or in_features
        hidden_features = hidden_features or in_features
        self.dense1 = nn.Dense(in_features, hidden_features)
        self.activation = activation()
        self.dense2 = nn.Dense(hidden_features, out_features)
        self.dropout = nn.Dropout(1 - drop_rate)

    def construct(self, x):
        """Feed Forward construct."""
        x = self.dense1(x)
        x = self.activation(x)
        x = self.dropout(x)
        x = self.dense2(x)
        x = self.dropout(x)

        return x


class ResidualCell(nn.Cell):

    def __init__(self, cell):
        super(ResidualCell, self).__init__()
        self.cell = cell

    def construct(self, x):
        """ResidualCell construct."""
        return self.cell(x) + x


class TransformerEncoder(nn.Cell):

    def __init__(self,
                 dim: int,
                 num_layers: int,
                 num_heads: int,
                 mlp_dim: int,
                 drop_rate: float = 0.,
                 attention_drop_rate: float = 0.,
                 drop_path_rate: float = 0.,
                 activation: nn.Cell = nn.GELU,
                 norm: nn.Cell = nn.LayerNorm):
        super(TransformerEncoder, self).__init__()
        dpr = [i.item() for i in np.linspace(0, drop_path_rate, num_layers)]
        attn_seeds = [np.random.randint(1024) for _ in range(num_layers)]
        mlp_seeds = [np.random.randint(1024) for _ in range(num_layers)]

        layers = []
        for i in range(num_layers):
            normalization1 = norm((dim,))
            normalization2 = norm((dim,))
            attention = Attention(dim=dim,
                                  num_heads=num_heads,
                                  drop_rate=drop_rate,
                                  attention_drop_rate=attention_drop_rate)

            feedforward = FeedForward(in_features=dim,
                                      hidden_features=mlp_dim,
                                      activation=activation,
                                      drop_rate=drop_rate)

            if drop_path_rate > 0:
                layers.append(
                    nn.SequentialCell([
                        ResidualCell(nn.SequentialCell([normalization1,
                                                        attention,
                                                        DropPath(dpr[i], attn_seeds[i])])),
                        ResidualCell(nn.SequentialCell([normalization2,
                                                        feedforward,
                                                        DropPath(dpr[i], mlp_seeds[i])]))]))
            else:
                layers.append(
                    nn.SequentialCell([
                        ResidualCell(nn.SequentialCell([normalization1,
                                                        attention])),
                        ResidualCell(nn.SequentialCell([normalization2,
                                                        feedforward]))
                    ])
                )
        self.layers = nn.SequentialCell(layers)

    def construct(self, x):
        """Transformer construct."""
        return self.layers(x)


class ViT(nn.Cell):

    def __init__(self,
                 image_size: int = 224,
                 input_channels: int = 3,
                 num_classes: int = 1000,
                 patch_size: int = 16,
                 embed_dim: int = 768,
                 num_layers: int = 12,
                 num_heads: int = 12,
                 mlp_dim: int = 3072,
                 drop_rate: float = 0.,
                 attention_drop_rate: float = 0.,
                 drop_path_rate: float = 0.,
                 activation: nn.Cell = nn.GELU,
                 norm: Optional[nn.Cell] = nn.LayerNorm,
                 pool: str = 'cls') -> None:
        super(ViT, self).__init__()
        self.num_features = embed_dim
        self.patch_embedding = PatchEmbedding(image_size=image_size,
                                              patch_size=patch_size,
                                              embed_dim=embed_dim,
                                              input_channels=input_channels)
        num_patches = self.patch_embedding.num_patches

        if pool == "cls":
            self.cls_token = Parameter(
                initializer(Normal(sigma=1.0), (1, 1, embed_dim), ms.float32),
                name='cls',
                requires_grad=True
            )

            self.pos_embedding = Parameter(
                initializer(Normal(sigma=1.0), (1, num_patches + 1, embed_dim), ms.float32),
                name='pos_embedding',
                requires_grad=True
            )
            self.concat = ops.Concat(axis=1)
        else:
            self.pos_embedding = Parameter(
                Parameter(Normal(sigma=1.0), (1, num_patches, embed_dim), ms.float32),
                name='pos_embedding',
                requires_grad=True
            )
            self.mean = ops.ReduceMean(keep_dims=False)

        self.pool = pool
        self.pos_dropout = nn.Dropout(1 - drop_rate)
        self.norm = norm((embed_dim,))
        self.tile = ops.Tile()
        self.transformer = TransformerEncoder(dim=embed_dim,
                                              num_layers=num_layers,
                                              num_heads=num_heads,
                                              mlp_dim=mlp_dim,
                                              drop_rate=drop_rate,
                                              attention_drop_rate=attention_drop_rate,
                                              drop_path_rate=drop_path_rate,
                                              activation=activation,
                                              norm=norm)

        self.classifier = nn.Dense(embed_dim, num_classes)

    def get_classifier(self):
        return self.classifier

    def reset_classifier(self, num_classes):
        self.classifier = nn.Dense(self.num_features, num_classes)

    def get_features(self, x):
        """ViT construct."""
        x = self.patch_embedding(x)

        if self.pool == "cls":
            cls_tokens = self.tile(self.cls_token, (x.shape[0], 1, 1))
            x = self.concat((cls_tokens, x))
            x += self.pos_embedding
        else:
            x += self.pos_embedding
        x = self.pos_dropout(x)
        x = self.transformer(x)
        x = self.norm(x)

        if self.pool == "cls":
            x = x[:, 0]
        else:
            x = self.mean(x, (1, 2))  # (1,) or (1,2)

        return x

    def construct(self, x):
        x = self.get_features(x)
        x = self.classifier(x)
        return x


@register_model
def vit_b_16_224(pretrained: bool = False, num_classes: int = 1000, in_channels=3, **kwargs):
    default_cfg = default_cfgs['vit_b_16_224']
    model = ViT(input_channels=in_channels,
                num_classes=num_classes,
                patch_size=16,
                embed_dim=768,
                num_layers=12,
                num_heads=12,
                mlp_dim=3072,
                **kwargs)
    model.dataset_transform = default_cfg['dataset_transform']

    if pretrained:
        load_pretrained(model, default_cfg, num_classes=num_classes, in_channels=in_channels)

    return model


@register_model
def vit_b_32_224(pretrained: bool = False, num_classes: int = 1000, in_channels=3, **kwargs):
    default_cfg = default_cfgs['vit_b_32_224']
    model = ViT(input_channels=in_channels,
                num_classes=num_classes,
                patch_size=32,
                embed_dim=768,
                num_layers=12,
                num_heads=12,
                mlp_dim=3072,
                **kwargs)
    model.dataset_transform = default_cfg['dataset_transform']

    if pretrained:
        load_pretrained(model, default_cfg, num_classes=num_classes, in_channels=in_channels)

    return model


@register_model
def vit_l_16_224(pretrained: bool = False, num_classes: int = 1000, in_channels=3, **kwargs):
    default_cfg = default_cfgs['vit_l_16_224']
    model = ViT(input_channels=in_channels,
                num_classes=num_classes,
                patch_size=16,
                embed_dim=1024,
                num_layers=24,
                num_heads=16,
                mlp_dim=4096,
                **kwargs)
    model.dataset_transform = default_cfg['dataset_transform']

    if pretrained:
        load_pretrained(model, default_cfg, num_classes=num_classes, in_channels=in_channels)

    return model


@register_model
def vit_l_32_224(pretrained: bool = False, num_classes: int = 1000, in_channels=3, **kwargs):
    default_cfg = default_cfgs['vit_l_32_224']
    model = ViT(input_channels=in_channels,
                num_classes=num_classes,
                patch_size=32,
                embed_dim=1024,
                num_layers=24,
                num_heads=16,
                mlp_dim=4096,
                **kwargs)
    model.dataset_transform = default_cfg['dataset_transform']

    if pretrained:
        load_pretrained(model, default_cfg, num_classes=num_classes, in_channels=in_channels)

    return model
